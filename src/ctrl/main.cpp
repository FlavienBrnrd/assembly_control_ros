#include <ros/ros.h>

#include <assembly_control_ros/ctrl_state.h>
#include <assembly_control_ros/ctrl_input.h>
#include <assembly_control_ros/ctrl_output.h>
#include <assembly_control_ros/ctrl_command.h>

/* ---- Include des HEADER sur les entrées/sorties de toutes les machines ----*/
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>
#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>
#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>

#include <common/machine_controller.hpp>

class Controller {
public:
    Controller(ros::NodeHandle node) : node_(node), state_(States::WAIT_PART) {

        /* ---- Declaration des publisher sur les machines ---- */
        pub_as_in =
            node.advertise<assembly_control_ros::assembly_station_input>(
                "/assembly_station/input", 10);
        pub_cam_in = node.advertise<assembly_control_ros::camera_input>(
            "/camera/input", 10);
        pub_ec_in =
            node.advertise<assembly_control_ros::evacuation_conveyor_input>(
                "/evacuation_conveyor/input", 10);
        pub_rob_in = node.advertise<assembly_control_ros::robot_input>(
            "/robot/input", 10);
        pub_sc_in = node.advertise<assembly_control_ros::supply_conveyor_input>(
            "/supply_conveyor/input", 10);

        /* ---- Declaration des subscriber sur les machines ---- */
        sub_cam_out = node.subscribe("/camera/output", 10,
                                     &Controller::camOutputsClbk, this);
        sub_rob_out = node.subscribe("/robot/output", 10,
                                     &Controller::robOutputsClbk, this);
        sub_ec_out = node.subscribe("/evacuation_conveyor/output", 10,
                                    &Controller::ecOutputsClbk, this);
        sub_as_out = node.subscribe("/assembly_sattion/output", 10,
                                    &Controller::asOutputsClbk, this);
        sub_sc_out = node.subscribe("/supply_conveyor/output", 10,
                                    &Controller::scOutputsClbk, this);
    }

    /* ---- Declaration etat de base du Controller ==> WAIT_PART ---- */
    virtual void process() {

        switch (state_) {
        case States::WAIT_PART:
            if (sc_o.part_available == true) {
                ROS_INFO("[Controleur] WAIT PART DONE");
                sc_o.part_available = false;
                cam_i.start_recognition = true;
                pub_cam_in.publish(cam_i);
                rob_i.go_left = false;
                rob_i.go_right = true;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_SC;
            }
            break;
        case States::WAIT_AT_SC:
            if ((cam_o.part_analyzed == true) && (rob_o.at_sc == true)) {
                ROS_INFO("[Controleur] WAIT AT SC DONE");
                rob_o.at_sc = false;
                cam_o.part_analyzed = false;
                rob_i.go_left = false;
                rob_i.go_right = false;
                rob_i.go_grasp = true;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_GRASP;
            }
            break;
        case States::WAIT_GRASP:
            if (rob_o.go_grasp_done == true) {
                ROS_INFO("[Controleur] WAIT GRASP DONE");
                rob_o.go_grasp_done = false;
                sc_i.restart = true;
                pub_sc_in.publish(sc_i);
                state_ = States::WAIT_DESCISION;
            }
            break;
        case States::WAIT_DESCISION:
            if ((stock1 == false) && (cam_o.part1 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go 1");
                rob_i.go_left = true;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_AS;
            }
            if ((stock2 == false) && (cam_o.part2 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go 2");
                rob_i.go_left = true;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_AS;
            }
            if ((stock3 == false) && (cam_o.part3 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go 3");
                rob_i.go_left = true;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_AS;
            }
            if ((stock1 == true) && (cam_o.part1 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go evac 1");
                rob_i.go_left = false;
                rob_i.go_right = true;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_EC;
            }
            if ((stock2 == true) && (cam_o.part2 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go evac 2");
                rob_i.go_left = false;
                rob_i.go_right = true;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_EC;
            }
            if ((stock3 == true) && (cam_o.part3 == true)) {
                ROS_INFO("[Controleur] WAIT DESCISION ==> go evac 3");
                rob_i.go_left = false;
                rob_i.go_right = true;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AT_EC;
            }
            break;
        case States::WAIT_AT_EC:
            if (rob_o.at_ec == true){
                ROS_INFO("[Controleur] WAIT AT EC DONE");
                rob_o.at_ec = false;
                ec_i.stop_ec = true;
                pub_ec_in.publish(ec_i);
                state_ = States::WAIT_EC_STOP;
            }
            break;
        case States::WAIT_EC_STOP:
            if (ec_o.ec_stopped == true) {
                ROS_INFO("[Controleur] WAIT EC STOP DONE");
                ec_o.ec_stopped = false;
                rob_i.go_left = false;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = true;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;

                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_RELEASE;
            }
            break;
        case States::WAIT_RELEASE:
            if (rob_o.go_release_done == true) {
                ROS_INFO("[Controleur] WAIT RELEASE DONE");
                rob_o.go_release_done = false;
                rob_i.go_left = true;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                ec_i.ec_restart = true;
                pub_ec_in.publish(ec_i);
                state_ = States::WAIT_SC_FROM_EC;
            }
            break;
        case States::WAIT_SC_FROM_EC:
            if ((rob_o.at_sc == true) && (rob_o.left_done == true)) {
                ROS_INFO("[Controleur] WAIT SC FROM EC DONE");
                rob_i.go_left = true;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AS_FROM_SC;
            }
            break;
        case States::WAIT_AS_FROM_SC:
            if ((rob_o.at_as == true) && (rob_o.left_done == true)) {
                ROS_INFO("[Controleur] WAIT AS FROM SC DONE");
                state_ = States::WAIT_PART;
            }
            break;
        case States::WAIT_AT_AS:
            if (rob_o.at_as == true) {
                ROS_INFO("[Controleur] WAIT AT AS DONE");
                state_ = States::WAIT_TYPE;
            }
            break;
        case States::WAIT_TYPE:
            if (cam_o.part1 == true) {
                ROS_INFO("[Controleur] WAIT TYPE DONE");
                cam_o.part1 = false;
                rob_i.go_left = false;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = true;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = false;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AS_1;
            } else if (cam_o.part2 == true) {
                ROS_INFO("[Controleur] WAIT TYPE DONE");

                cam_o.part2 = false;
                rob_i.go_left = false;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = true;
                rob_i.go_assemble_3 = false;

                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AS_2;
            } else if (cam_o.part3 == true) {
                ROS_INFO("[Controleur] WAIT TYPE DONE");
                cam_o.part3 = false;
                rob_i.go_left = false;
                rob_i.go_right = false;
                rob_i.go_grasp = false;
                rob_i.go_release = false;
                rob_i.go_assemble_1 = false;
                rob_i.go_assemble_2 = false;
                rob_i.go_assemble_3 = true;
                pub_rob_in.publish(rob_i);
                state_ = States::WAIT_AS_3;
            }
            break;
        case States::WAIT_AS_1:
            if (rob_o.part1_done == true) {
                ROS_INFO("[Controleur] WAIT ASSEMBLE 1 DONE");
                rob_o.part1_done = false;
                stock1 = true;
                state_ = States::WAIT_ALL_DONE;
            }
            break;
        case States::WAIT_AS_2:
            if (rob_o.part2_done == true) {
                ROS_INFO("[Controleur] WAIT ASSEMBLE 2 DONE");
                rob_o.part2_done = false;
                stock2 = true;
                state_ = States::WAIT_ALL_DONE;
            }
            break;
        case States::WAIT_AS_3:
            if (rob_o.part3_done == true) {
                ROS_INFO("[Controleur] WAIT ASSEMBLE 3 DONE");
                rob_o.part3_done = false;
                stock3 = true;
                state_ = States::WAIT_ALL_DONE;
            }
            break;
        case States::WAIT_ALL_DONE:
            ROS_INFO("[Controleur] WAIT FOR OTHER PART");
            if ((stock1 == true) && (stock2 == true) && (stock3 == true)) {
            ROS_INFO("[Controleur] ALL PARTS DONE");
                stock1 = false;
                stock2 = false;
                stock3 = false;
                as_i.partsdone = true;
                pub_as_in.publish(as_i);
            }
            state_ = States::GO_WAIT;
            break;
        case States::GO_WAIT:
            ROS_INFO("[Controleur] GO WAIT");
            state_ = States::WAIT_PART;
            break;
        default:
            break;
        }
    }

protected:
    /* ---- declaration des message à lire sur les topics OUTPUTS ----*/
    assembly_control_ros::assembly_station_output as_o;
    assembly_control_ros::camera_output cam_o;
    assembly_control_ros::robot_output rob_o;
    assembly_control_ros::evacuation_conveyor_output ec_o;
    assembly_control_ros::supply_conveyor_output sc_o;

    /* ---- declaration des message à lire sur les topics INPUTS ----*/
    assembly_control_ros::assembly_station_input as_i;
    assembly_control_ros::camera_input cam_i;
    assembly_control_ros::robot_input rob_i;
    assembly_control_ros::evacuation_conveyor_input ec_i;
    assembly_control_ros::supply_conveyor_input sc_i;

private:
    ros::NodeHandle node_;

    bool stock1 = false;
    bool stock2 = false;
    bool stock3 = false;
    
    /* ---- Enumeration pour la machine à etat ---- */
    enum States {
        WAIT_PART,
        WAIT_AT_SC,
        WAIT_AT_EC,
        WAIT_AT_AS,
        WAIT_GRASP,
        WAIT_DESCISION,
        WAIT_EC_STOP,
        WAIT_RELEASE,
        WAIT_SC_FROM_EC,
        WAIT_AS_FROM_SC,
        WAIT_TYPE,
        WAIT_AS_3,
        WAIT_AS_2,
        WAIT_AS_1,
        WAIT_ALL_DONE,
        GO_WAIT
    };
    States state_;

    /* ---- fonction de callback sur les outputs des machines ----*/
    void asOutputsClbk(
        const assembly_control_ros::assembly_station_output::ConstPtr& msg) {
        //callback sur la machine assembly station
        as_o = *msg;
    }
    void ecOutputsClbk(
        const assembly_control_ros::evacuation_conveyor_output::ConstPtr& msg) {
        //callback sur la machine evacuation conveyor
        ec_o = *msg;
    }
    void
    robOutputsClbk(const assembly_control_ros::robot_output::ConstPtr& msg) {
        //callback sur la machine robot
        rob_o = *msg;
    }
    void
    camOutputsClbk(const assembly_control_ros::camera_output::ConstPtr& msg) {
        //callback sur la machine camera
        cam_o = *msg;
    }
    void scOutputsClbk(
        const assembly_control_ros::supply_conveyor_output::ConstPtr& msg) {
        //callback sur la machine supply conveyor
        sc_o = *msg;
    }

    /* ---- Declaration des publisher sur les machines ---- */
    ros::Publisher pub_as_in;
    ros::Publisher pub_cam_in;
    ros::Publisher pub_ec_in;
    ros::Publisher pub_rob_in;
    ros::Publisher pub_sc_in;

    /* ---- Declaration des subscriber sur les machines ---- */
    ros::Subscriber sub_cam_out;
    ros::Subscriber sub_rob_out;
    ros::Subscriber sub_ec_out;
    ros::Subscriber sub_as_out;
    ros::Subscriber sub_sc_out;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controller");
    ros::NodeHandle node_;
    ros::Rate loop_rate(50); // 50 Hz

    Controller master(node_);
    
    while (ros::ok()) {
        master.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
    return 0;
}