#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>
#include <common/machine_controller.hpp>

class Robot : public MachineController<assembly_control_ros::robot_state,
                                       assembly_control_ros::robot_input,
                                       assembly_control_ros::robot_command,
                                       assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::ROB_WAIT) {
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();

        switch (state_) {
        case State::ROB_WAIT:
            if (inputs.go_left == true) {
                state_ = State::ROB_MOVE_LEFT;
            }
            if (inputs.go_right == true) {
                state_ = State::ROB_MOVE_RIGHT;
            }
            if (inputs.go_grasp == true) {
                state_ = State::ROB_GRASP;
            }
            if (inputs.go_release == true) {
                state_ = State::ROB_RELEASE;
            }
            if (inputs.go_assemble_1 == true) {
                state_ = State::ROB_ASSEMBLE_1;
            }
            if (inputs.go_assemble_2 == true) {
                state_ = State::ROB_ASSEMBLE_2;
            }
            if (inputs.go_assemble_3 == true) {
                state_ = State::ROB_ASSEMBLE_3;
            }
            break;

        case State::ROB_MOVE_RIGHT:
            state_ = State::INFO_RIGHT;
            break;
        case State::INFO_RIGHT:
            commands.move_right = true;
            if ((getState().at_supply_conveyor) && (current_pos_as == true)) {
                current_pos_as = false;
                current_pos_ec = false;
                current_pos_sc = true;
                commands.move_right = false;
                inputs.go_right = false;
                outputs.go_grasp_done = false;
                outputs.go_release_done = false;
                outputs.part1_done = false;
                outputs.part2_done = false;
                outputs.part3_done = false;
                outputs.right_done = true;
                outputs.left_done = false;
                outputs.at_sc = true;
                outputs.at_ec = false;
                outputs.at_as = false;
                sendOuputs(outputs);
                state_ = State::ROB_WAIT;
            } else if ((getState().at_evacuation_conveyor) &&
                       (current_pos_sc == true)) {
                current_pos_as = false;
                current_pos_ec = true;
                current_pos_sc = false;
                commands.move_right = false;
                inputs.go_right = false;
                outputs.go_grasp_done = false;
                outputs.go_release_done = false;
                outputs.part1_done = false;
                outputs.part2_done = false;
                outputs.part3_done = false;
                outputs.right_done = true;
                outputs.left_done = false;
                outputs.at_sc = false;
                outputs.at_ec = true;
                outputs.at_as = false;
                sendOuputs(outputs);
                state_ = State::ROB_WAIT;
            }
            break;
        case State::ROB_MOVE_LEFT:
            state_ = State::INFO_LEFT;
            break;
        case State::INFO_LEFT:
            commands.move_left = true;
            if ((getState().at_supply_conveyor) && (current_pos_ec == true)) {
                current_pos_as = false;
                current_pos_ec = false;
                current_pos_sc = true;
                commands.move_left = false;
                inputs.go_left = false;
                outputs.go_grasp_done = false;
                outputs.go_release_done = false;
                outputs.part1_done = false;
                outputs.part2_done = false;
                outputs.part3_done = false;
                outputs.right_done = false;
                outputs.left_done = true;
                outputs.at_as = false;
                outputs.at_ec = false;
                outputs.at_sc = true;
                sendOuputs(outputs);
                state_ = State::ROB_WAIT;
            } else if ((getState().at_assembly_station) &&
                       (current_pos_sc == true)) {
                current_pos_as = true;
                current_pos_ec = false;
                current_pos_sc = false;
                commands.move_left = false;
                inputs.go_left = false;
                outputs.go_grasp_done = false;
                outputs.go_release_done = false;
                outputs.part1_done = false;
                outputs.part2_done = false;
                outputs.part3_done = false;
                outputs.right_done = false;
                outputs.left_done = true;
                outputs.at_sc = false;
                outputs.at_ec = false;
                outputs.at_as = true;
                sendOuputs(outputs);
                state_ = State::ROB_WAIT;
            }
            break;
        case State::ROB_GRASP:
            commands.grasp = true;
            if (getState().part_grasped == true) {
                commands.grasp = false;
                outputs.go_grasp_done = true;
                sendOuputs(outputs);
                ROS_INFO("[Robot : Grasped]");
                inputs.go_grasp = false;
                state_ = State::ROB_WAIT;
            }
            break;
        case State::ROB_RELEASE:
            commands.release = true;
            if (getState().part_released == true) {
                commands.release = false;
                outputs.go_release_done = true;
                sendOuputs(outputs);
                ROS_INFO("[Robot : Released]");
                inputs.go_release = false;
                state_ = State::ROB_WAIT;
            }

            break;
        case State::ROB_ASSEMBLE_1:
            commands.assemble_part1 = true;
            if ((inputs.go_assemble_1 == true) &&
                (getState().part_grasped == false)) {
                commands.assemble_part1 = false;

                outputs.part1_done = true;
                inputs.go_assemble_1 = false;
                sendOuputs(outputs);
                ROS_INFO("[Robot : Assemble part 1]");
                state_ = State::ROB_WAIT;
            }
            break;
        case State::ROB_ASSEMBLE_2:
            commands.assemble_part2 = true;
            if ((inputs.go_assemble_2 == true) &&
                (getState().part_grasped == false)) {
                commands.assemble_part2 = false;

                outputs.part2_done = true;
                inputs.go_assemble_2 = false;
                sendOuputs(outputs);
                ROS_INFO("[Robot : Assemble part 2]");
                state_ = State::ROB_WAIT;
            }
            break;
        case State::ROB_ASSEMBLE_3:
            commands.assemble_part3 = true;
            if ((inputs.go_assemble_3 == true) &&
                (getState().part_grasped == false)) {
                commands.assemble_part3 = false;

                outputs.part3_done = true;
                inputs.go_assemble_3 = false;
                sendOuputs(outputs);
                ROS_INFO("[Robot : Assemble part 3]");
                state_ = State::ROB_WAIT;
            }
            break;
        default:
            break;
        }
        sendCommands(commands);
    }

private:
    enum class State {
        ROB_WAIT,
        ROB_MOVE_LEFT,
        ROB_MOVE_RIGHT,
        ROB_RELEASE,
        ROB_GRASP,
        ROB_ASSEMBLE_1,
        ROB_ASSEMBLE_2,
        ROB_ASSEMBLE_3,
        INFO_LEFT,
        INFO_RIGHT
    };
    State state_;
    /* ---- Le robot est à L'assembly station au départ ---- */
    bool current_pos_as = true;
    bool current_pos_ec = false;
    bool current_pos_sc = false;
};

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}