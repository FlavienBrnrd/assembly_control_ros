#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>
#include <common/machine_controller.hpp>

class EvacuationConveyor
    : public MachineController<
          assembly_control_ros::evacuation_conveyor_state,
          assembly_control_ros::evacuation_conveyor_input,
          assembly_control_ros::evacuation_conveyor_command,
          assembly_control_ros::evacuation_conveyor_output> {
public:
    EvacuationConveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::EC_ON) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();

        switch (state_) {
        case State::EC_ON:
            commands.on = true;
            if (inputs.stop_ec == true) {
                inputs.stop_ec = false;
                ROS_INFO("[EvacuationConveyor : Stop]");
                outputs.ec_stopped = true;
                sendOuputs(outputs);
                commands.on = false;
                state_ = State::EC_OFF;
            } 
            break;
        case State::EC_OFF:
            if (inputs.ec_restart == true) {
                inputs.ec_restart = false;
                commands.on = true;
                ROS_INFO("[EvacuationConveyor : Start]");
                state_ = State::EC_ON;
            }
            
        default:
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { EC_ON, EC_OFF };
    State state_;
};

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    EvacuationConveyor conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}