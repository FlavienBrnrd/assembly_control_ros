#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_output> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::AS_CHECK) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();
        switch (state_) {
        case State::AS_CHECK:
            commands.check = true;         
            if (inputs.partsdone == true) {
                inputs.partsdone = false;
                commands.check = false;
                if (getState().valid == true) {
                    ROS_INFO("[Assembly Station] Assembly Valid Go Evac");
                    state_ = State::AS_EVAC;
                }
            }
            break;
        case State::AS_EVAC:
            if ((getState().evacuated == true)) {
                ROS_INFO("[Assembly Station] Evacuation Done");
                state_ = State::AS_CHECK;
            }
            break;
        }
        sendCommands(commands);
    }

private:
    enum class State {
        AS_EVAC,
        AS_CHECK,
    };

    State state_;
};

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}