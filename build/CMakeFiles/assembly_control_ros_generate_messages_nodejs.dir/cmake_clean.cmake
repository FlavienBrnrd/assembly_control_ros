file(REMOVE_RECURSE
  "CMakeFiles/assembly_control_ros_generate_messages_nodejs"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/status_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/ctrl_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/robot_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/ctrl_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/supply_conveyor_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/camera_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/supply_conveyor_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/camera_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/robot_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/supply_conveyor_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/camera_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/evacuation_conveyor_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/ctrl_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/ctrl_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/evacuation_conveyor_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/status_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/supply_conveyor_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/robot_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/status_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/assembly_station_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/camera_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/robot_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/evacuation_conveyor_input.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/evacuation_conveyor_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/status_output.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/assembly_station_state.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/assembly_station_command.js"
  "devel/share/gennodejs/ros/assembly_control_ros/msg/assembly_station_input.js"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/assembly_control_ros_generate_messages_nodejs.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
