file(REMOVE_RECURSE
  "CMakeFiles/assembly_control_ros_generate_messages_cpp"
  "devel/include/assembly_control_ros/status_input.h"
  "devel/include/assembly_control_ros/ctrl_output.h"
  "devel/include/assembly_control_ros/robot_command.h"
  "devel/include/assembly_control_ros/ctrl_state.h"
  "devel/include/assembly_control_ros/supply_conveyor_state.h"
  "devel/include/assembly_control_ros/camera_input.h"
  "devel/include/assembly_control_ros/supply_conveyor_command.h"
  "devel/include/assembly_control_ros/camera_command.h"
  "devel/include/assembly_control_ros/robot_output.h"
  "devel/include/assembly_control_ros/supply_conveyor_output.h"
  "devel/include/assembly_control_ros/camera_output.h"
  "devel/include/assembly_control_ros/evacuation_conveyor_state.h"
  "devel/include/assembly_control_ros/ctrl_command.h"
  "devel/include/assembly_control_ros/ctrl_input.h"
  "devel/include/assembly_control_ros/evacuation_conveyor_command.h"
  "devel/include/assembly_control_ros/status_state.h"
  "devel/include/assembly_control_ros/supply_conveyor_input.h"
  "devel/include/assembly_control_ros/robot_input.h"
  "devel/include/assembly_control_ros/status_command.h"
  "devel/include/assembly_control_ros/assembly_station_output.h"
  "devel/include/assembly_control_ros/camera_state.h"
  "devel/include/assembly_control_ros/robot_state.h"
  "devel/include/assembly_control_ros/evacuation_conveyor_input.h"
  "devel/include/assembly_control_ros/evacuation_conveyor_output.h"
  "devel/include/assembly_control_ros/status_output.h"
  "devel/include/assembly_control_ros/assembly_station_state.h"
  "devel/include/assembly_control_ros/assembly_station_command.h"
  "devel/include/assembly_control_ros/assembly_station_input.h"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/assembly_control_ros_generate_messages_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
