file(REMOVE_RECURSE
  "CMakeFiles/assembly_control_ros_generate_messages_py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_status_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_ctrl_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_robot_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_ctrl_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_supply_conveyor_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_camera_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_supply_conveyor_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_camera_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_robot_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_supply_conveyor_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_camera_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_evacuation_conveyor_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_ctrl_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_ctrl_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_evacuation_conveyor_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_status_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_supply_conveyor_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_robot_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_status_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_assembly_station_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_camera_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_robot_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_evacuation_conveyor_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_evacuation_conveyor_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_status_output.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_assembly_station_state.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_assembly_station_command.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/_assembly_station_input.py"
  "devel/lib/python2.7/dist-packages/assembly_control_ros/msg/__init__.py"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/assembly_control_ros_generate_messages_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
