#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/flavien/catkin_ws2/src/assembly_control_ros/build/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/flavien/catkin_ws2/src/assembly_control_ros/build/devel/lib:$LD_LIBRARY_PATH"
export PATH="/opt/ros/melodic/bin:/home/flavien/.cargo/bin:/home/flavien/.local/bin:/home/flavien/.cargo/bin:/home/flavien/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/home/flavien/catkin_ws2/src/assembly_control_ros/build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/flavien/catkin_ws2/src/assembly_control_ros/build"
export PYTHONPATH="/home/flavien/catkin_ws2/src/assembly_control_ros/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/flavien/catkin_ws2/src/assembly_control_ros/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/flavien/catkin_ws2/src/assembly_control_ros:$ROS_PACKAGE_PATH"